#!/bin/bash

realpath() {  # for MAC OS
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

say_help() {
	echo "Using: $0 USER_UID USER_GID"
}

if [[ ! $# == 2 ]]; then
	say_help
	exit
fi

USER_UID=$1
USER_GID=$2

if [ $USER_UID -eq 0 ] || [ $USER_GID -eq 0 ]; then
    echo "You are trying to create non-root user with UID=$USER_UID and GID=$USER_GID. You probably doing something wrong"
    exit 1
fi

cd "$(dirname $(realpath $0))"
docker build --build-arg USER_UID=$USER_UID --build-arg USER_GID=$USER_GID -t markovvn1/ros_gaz_env_eurobot2021 .