#include <behavior_tree/wrap_nodes/conditions/is_condition_template.h>

BT::PortsList IsTemplate::providedPorts()
{
    return {
        BT::InputPort<double>("parameter")
    };
}

void IsTemplate::sendRequest(RequestType& request)
{
    auto value = getInput<double>("parameter");
    if (!value)
        ROS_ERROR("IsTemplate has incorrect port value ");

    request.parameter = value.value();
}

BT::NodeStatus IsTemplate::onResponse(const ResponseType& rep)
{
    if (rep.res == false)
        return BT::NodeStatus::FAILURE;
    return BT::NodeStatus::SUCCESS;
}

BT::NodeStatus IsTemplate::onFailedRequest(RosServiceNode::FailureCause failure)
{
    ROS_ERROR("IsTemplate request failed %d", static_cast<int>(failure));
    return BT::NodeStatus::FAILURE;
}