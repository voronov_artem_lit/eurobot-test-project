#include <behavior_tree/wrap_nodes/actions/action_template.h>

BT::PortsList Template::providedPorts()
{
    return {
        BT::InputPort<std::string>("side")
    };
}

bool Template::sendGoal(GoalType& goal)
{
    auto value = getInput<std::string>("side").value();

    if( value == "SIDE_YELLOW" ){
        goal.side = goal.SIDE_YELLOW;
        return true;
    }

    if( value == "SIDE_BLUE" ){
        goal.side = goal.SIDE_BLUE;
        return true;
    }

    ROS_ERROR("Template has incorrect port value '%s'", value.c_str());
    return false;
}

BT::NodeStatus Template::onResult( const ResultType& res)
{
    if (res.res == true)
        return BT::NodeStatus::SUCCESS;
    return BT::NodeStatus::FAILURE;
}

BT::NodeStatus Template::onFailedRequest(FailureCause failure)
{
    ROS_ERROR("Template request failed %d", static_cast<int>(failure));
    return BT::NodeStatus::FAILURE;
}

void Template::halt()
{
    if( status() == BT::NodeStatus::RUNNING ) {
        ROS_WARN("Template aborted(halted)");
        BaseClass::halt();
    }
}