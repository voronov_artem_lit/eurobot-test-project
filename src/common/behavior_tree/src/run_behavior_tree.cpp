#include <ros/ros.h>
#include <ros/package.h>

#include <behaviortree_cpp_v3/bt_factory.h>
#include <behaviortree_cpp_v3/loggers/bt_minitrace_logger.h>
#include <behaviortree_cpp_v3/loggers/bt_zmq_publisher.h>
#include <behaviortree_cpp_v3/loggers/bt_file_logger.h>
#include <behavior_tree/logger/rosout_logger.h>
#include <behavior_tree/wrap_nodes/wrap_node.h>

#include <vector>
#include <string>
#include <utility>
#include <ctime>

#include <eurobot/utils/ros/get_param_list_with_log_error.hpp>
#include <eurobot/utils/current_date_time.hpp>


int main(int argc, char **argv)
{
    ros::init(argc, argv, "behavior_tree");

    ROS_FATAL_COND(argc != 2, "No BT '.xml' path argument in command-line options");
    const std::string& path_to_tree = argv[1];

    ros::NodeHandle nh;
    BT::BehaviorTreeFactory factory;
    std::pair<ros::V_string, ros::V_string> node;    // node_name + topic_path

    const ros::V_string params_act_list_name = {
        "template",
        // ...
    };
    const ros::V_string params_con_list_name = {
        "isTemplate",
        // ...
    };
    ros::V_string action_params = get_param_list_with_log_error(nh, params_act_list_name,    "actions/");
    ros::V_string condit_params = get_param_list_with_log_error(nh, params_con_list_name, "conditions/");

    BT::RegisterRosAction <Template> (factory, nh, "template", action_params[0]);

    BT::RegisterRosService<IsTemplate>  (factory, nh, "isTemplate", condit_params[0]);

    auto tree = factory.createTreeFromFile(path_to_tree);
    
    ROS_INFO("#####BEHAVIOR_TREE_START#####");
    /// LOGGERS
    printTreeRecursively(tree.rootNode());
    BT::RosoutLogger logger(tree.rootNode());  
    logger.enableTransitionToIdle(false);

    // This logger saves state changes on file
    BT::FileLogger logger_file(
                                tree,
                                std::string(currentDateTime() + "_BT_trace.fbl").c_str(),
                                20
                            );

    // server name: robot1-eurobot
    BT::PublisherZMQ publisher_zmq ( 
                                tree, // const BT::Tree& tree
                                50,   // unsigned max_msg_per_second
                                1666, // unsigned publisher_port 
                                1667  // unsigned server_port
                            );
    publisher_zmq.enableTransitionToIdle(true);

    BT::NodeStatus status = BT::NodeStatus::RUNNING;
    while(ros::ok() && (status == BT::NodeStatus::IDLE || status == BT::NodeStatus::RUNNING)){
        ros::spinOnce();
        status = tree.tickRoot();
        ros::Duration sleep_time(0.01);
        sleep_time.sleep();
    }
    ROS_INFO("#####BEHAVIOR_TREE_FINISH#####");

    return 0;
}