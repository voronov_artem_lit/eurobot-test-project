#pragma once

#include <behaviortree_cpp_v3/behavior_tree.h>
#include <geometry_msgs/PoseStamped.h>
#include <ros/ros.h>

#include <string>

namespace BT
{
// Template specialization to converts a string to geometry_msgs::PoseStampedPtr
/**
 * @brief Parse XML string to geometry_msgs::msg::PoseStampedPtr
 * @param key XML string
 * @return geometry_msgs::msg::PoseStamped
 */
template <> inline
geometry_msgs::PoseStampedPtr convertFromString <geometry_msgs::PoseStampedPtr> (const StringView str)
{
    geometry_msgs::PoseStampedPtr output;
    output->header.stamp = ros::Time::now();
    // We expect real numbers separated by semicolons
    auto parts = splitString(str, ';');
    if (parts.size() == 3)
    {
        output->header.frame_id = BT::convertFromString<std::string>(parts[0]);
        auto& pos = output->pose.position;
        pos.x = convertFromString<double>(parts[1]);
        pos.y = convertFromString<double>(parts[2]);
        pos.z = 0;
        return output;
    }
    if (parts.size() == 4)
    {
        output->header.frame_id = BT::convertFromString<std::string>(parts[0]);
        auto& pos = output->pose.position;
        pos.x = convertFromString<double>(parts[1]);
        pos.y = convertFromString<double>(parts[2]);
        pos.z = convertFromString<double>(parts[3]);
        return output;
    }

    ROS_ERROR("invalid input while converts a string to geometry_msgs::PoseStampedPtr\n");
    throw LogicError(std::string("invalid input while converts a string to geometry_msgs::PoseStampedPtr "));
}

// Template specialization to converts a string to geometry_msgs::PoseStamped
/**
 * @brief Parse XML string to geometry_msgs::msg::PoseStamped
 * @param key XML string
 * @return geometry_msgs::msg::PoseStamped
 */
template <> inline
geometry_msgs::PoseStamped convertFromString <geometry_msgs::PoseStamped> (const StringView str)
{
    geometry_msgs::PoseStamped output;
    output.header.stamp = ros::Time::now();

    // We expect real numbers separated by semicolons
    auto parts = splitString(str, ';');
    if (parts.size() == 3)
    {
        output.header.frame_id = BT::convertFromString<std::string>(parts[0]);
        auto& pos = output.pose.position;
        pos.x = convertFromString<double>(parts[1]);
        pos.y = convertFromString<double>(parts[2]);
        pos.z = 0;
        return output;
    }
    if (parts.size() == 4)
    {
        output.header.frame_id = BT::convertFromString<std::string>(parts[0]);
        auto& pos = output.pose.position;
        pos.x = convertFromString<double>(parts[1]);
        pos.y = convertFromString<double>(parts[2]);
        pos.z = convertFromString<double>(parts[3]);
        return output;
    }
    ROS_ERROR("invalid input while converts a string to geometry_msgs::PoseStamped\n");
    throw LogicError(std::string("invalid input while converts a string to geometry_msgs::PoseStamped "));
}
} // end namespace BT