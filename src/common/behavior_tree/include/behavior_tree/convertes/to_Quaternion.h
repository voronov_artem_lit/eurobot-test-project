#pragma once

#include <behaviortree_cpp_v3/behavior_tree.h>
#include <geometry_msgs/Quaternion.h>

#include <string>

namespace BT
{
/**
 * @brief Parse XML string to geometry_msgs::msg::Quaternion
 * @param key XML string
 * @return geometry_msgs::msg::Quaternion
 */
template<> inline
geometry_msgs::Quaternion convertFromString(const StringView key)
{
    // four real numbers separated by semicolons
    auto parts = BT::splitString(key, ';');
    if (parts.size() != 4) {
        throw std::runtime_error("invalid number of fields for orientation attribute)");
    } else {
        geometry_msgs::Quaternion orientation;
        orientation.x = BT::convertFromString<double>(parts[0]);
        orientation.y = BT::convertFromString<double>(parts[1]);
        orientation.z = BT::convertFromString<double>(parts[2]);
        orientation.w = BT::convertFromString<double>(parts[3]);
        return orientation;
    }
}
} // end namespace BT