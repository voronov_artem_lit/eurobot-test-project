// Copyright (c) 2019 Samsung Research America
// Copyright (c) 2020 Davide Faconti
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once

#include <behaviortree_cpp_v3/action_node.h>
#include <behaviortree_cpp_v3/bt_factory.h>
#include <ros/ros.h>
#include <ros/service_client.h>
#include <behavior_tree/convertes/bt_conversions.h>

#include <string>
#include <memory>

namespace BT
{

/**
 * Base Action to implement a ROS Service
 */
template<class ServiceT>
class RosServiceNode : public BT::SyncActionNode
{
protected:
  ros::ServiceClient service_client_;
  typename ServiceT::Response reply_;

  // The node that will be used for any ROS operations
  ros::NodeHandle& node_;

public:

  using BaseClass    = RosServiceNode<ServiceT>;
  using ServiceType  = ServiceT;
  using RequestType  = typename ServiceT::Request;
  using ResponseType = typename ServiceT::Response;
  
  enum FailureCause{
    MISSING_SERVER  = 0,
    FAILED_CALL     = 1
  };
  
  RosServiceNode() = delete;
  virtual ~RosServiceNode() = default;

  /// These ports will be added automatically if this Node is
  /// registered using RegisterRosAction<DeriveClass>()
  static PortsList providedPorts();

  /// User must implement this method.
  virtual void sendRequest(RequestType& request) = 0;

  /// Method (to be implemented by the user) to receive the reply.
  /// User can decide which NodeStatus it will return (SUCCESS or FAILURE).
  virtual NodeStatus onResponse( const ResponseType& rep) = 0;

  /// Called when a service call failed. Can be overriden by the user.
  virtual NodeStatus onFailedRequest(FailureCause failure);

protected:
  RosServiceNode(ros::NodeHandle& nh, const std::string& name, const std::string& service_name, const BT::NodeConfiguration & conf);
  BT::NodeStatus tick() override;
};


/// Method to register the service into a factory.
/// It gives you the opportunity to set the ros::NodeHandle.
template <class DerivedT> static
  void RegisterRosService(BT::BehaviorTreeFactory& factory,
                    ros::NodeHandle& node_handle,
                    const std::string& registration_ID,
                    const std::string& service_name)
{
  NodeBuilder builder = [&node_handle, &service_name](const std::string& name, const NodeConfiguration& config) {
    return std::make_unique<DerivedT>(node_handle, name, service_name, config );
  };

  TreeNodeManifest manifest;
  manifest.type = getType<DerivedT>();
  manifest.ports = DerivedT::providedPorts();
  manifest.registration_ID = registration_ID;
  const auto& basic_ports = RosServiceNode< typename DerivedT::ServiceType>::providedPorts();
  manifest.ports.insert( basic_ports.begin(), basic_ports.end() );

  factory.registerBuilder( manifest, builder );
}

template<class ServiceT>
RosServiceNode<ServiceT>::RosServiceNode(ros::NodeHandle& nh, const std::string& name, const std::string& service_name, const BT::NodeConfiguration & conf)
    :BT::SyncActionNode(name, conf), node_(nh) 
{ 
  service_client_ = node_.serviceClient<ServiceT>( service_name );
  ROS_INFO("'%s': waitForExistence", name.c_str());
  service_client_.waitForExistence();
  ROS_INFO("'%s': isReady", name.c_str());

}

template<class ServiceT>
PortsList RosServiceNode<ServiceT>::providedPorts()
{
  return  {
    InputPort<unsigned>("timeout", 100, "timeout to connect to server (milliseconds)")
  };
}

template<class ServiceT>
NodeStatus RosServiceNode<ServiceT>::onFailedRequest(RosServiceNode::FailureCause failure)
{
  return NodeStatus::FAILURE;
}

template<class ServiceT>
BT::NodeStatus RosServiceNode<ServiceT>::tick()
{
  unsigned msec = getInput<unsigned>("timeout").value();
  ros::Duration timeout(static_cast<double>(msec) * 1e-3);
  
  bool connected = service_client_.waitForExistence(timeout);
  if( !connected ) {return onFailedRequest(MISSING_SERVER);}
  
  typename ServiceT::Request request;
  sendRequest(request);
  bool received = service_client_.call( request, reply_ );
  if( !received ) {return onFailedRequest(FAILED_CALL);}

  return onResponse(reply_);
}

}  // namespace BT
