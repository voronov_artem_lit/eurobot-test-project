#!/usr/bin/env python3

"""Шаблон модуля высокоуровневого контроля робота"""

import rospy
from eurobot.core import PathResolver

from con_is_template.srv import IsTemplate, IsTemplateRequest, IsTemplateResponse


class IsTemplateNode:
    """Main class."""

    def __init__(self):
        """Create main class."""
        resolve_path = PathResolver()
        robot_parameter = resolve_path(rospy.get_param("parameter"))
        robot_parameter

        self._server = rospy.Service(
            "command", IsTemplate, self.server_callback)

    def start(self):
        """Subscribe to all neaded topics."""
        rospy.loginfo("Main for condition 'is_template' is loaded")

    def server_callback(self, req: IsTemplateRequest) -> IsTemplateResponse:
        """Callback for server."""
        return IsTemplateResponse(True)


def main():
    """Main entrypoint."""
    rospy.init_node("is_template", anonymous=True)

    main = IsTemplateNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
