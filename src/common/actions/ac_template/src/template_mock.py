#!/usr/bin/env python3

"""Шаблон для модуля высокоуровневого контроля робота"""

import time
import rospy
from eurobot.core import PathResolver
import actionlib

import ac_template.msg


class Template:
    """Wrapper for action."""
    Action = ac_template.msg.TemplateAction
    Feedback = ac_template.msg.TemplateFeedback
    Goal = ac_template.msg.TemplateGoal
    Result = ac_template.msg.TemplateResult


class TemplateNode:
    """Main class."""

    _result = Template.Result()

    def __init__(self):
        """Create main class."""
        self.resolve_path = PathResolver()

        self._action_server = actionlib.SimpleActionServer(
            "command",
            Template.Action,
            execute_cb=self.action_callback,
            auto_start=False)

    def start(self):
        """Subscribe to all neaded topics."""
        self._action_server.start()
        rospy.logwarn("Mock for action 'template' is loaded")

    def action_callback(self, goal: Template.Goal):
        """Callback for action."""
        time.sleep(4)
        self._result.res = True
        self._action_server.set_succeeded(self._result)


def main():
    """Main entrypoint."""
    rospy.init_node("template", anonymous=True)

    main = TemplateNode()
    main.start()

    rospy.spin()


if __name__ == "__main__":
    main()
