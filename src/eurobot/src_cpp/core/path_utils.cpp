#include <eurobot/core/path_utils.hpp>

std::string join_paths(const std::string& a, const std::string& b)
{
    if (a.length() == 0) return b;
    if (b.length() == 0) return a;

    bool a_has_slash = a[a.length() - 1] == '/';
    bool b_has_slash = b[0] == '/';

    if (a_has_slash && b_has_slash)
        return a + b.substr(1);
    if (a_has_slash || b_has_slash)
        return a + b;
    return a + '/' + b;
}

std::string join_paths(const std::string& a, const std::string& b, const std::string& c)
{
    return join_paths(join_paths(a, b), c);
}



void PathResolver::init(ros::NodeHandle& nh)
{
    if (nh.hasParam("home_path"))
    {
        nh.getParam("home_path", home_path);
    }
    else
    {
        ROS_WARN("Parameter 'home_path' not found. Either create this parameter or do not call .init()");
    }
}

std::string PathResolver::operator ()(const std::string& path)
{
    if (path.length() == 0) return path;
    if (home_path.length() == 0) return path;

    if (path[0] == '/') return path; // absolute path

    if (path.length() >= 2 && path[0] == '~' && path[1] == '/') // relative to home path
    {
        if (home_path[home_path.length() - 1] == '/')
            return home_path + path.substr(2);
        return home_path + path.substr(1);
    }

    return path; // relative path
}
