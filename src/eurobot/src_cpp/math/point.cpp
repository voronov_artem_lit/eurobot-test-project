#include <cmath>
#include <eurobot/math/linalg.hpp>


Point::Point() : Point(0) {}
Point::Point(int size, double fill) : Vector(size, fill) {}
Point::Point(const Vector& v) : Vector(v) {}

void Point::assert_y(int y) const
{
    if (y < 0 || y >= rows)
        throw std::runtime_error("Point::assert_y(): y < 0 || y >= rows");
}

double& Point::at(int y) { assert_y(y); return data[y]; }
double Point::at(int y) const { assert_y(y); return data[y]; }

std::vector<double>& Point::get_data() { return data; }
std::vector<double> Point::get_data() const { return data; }

Vector Point::to_vector() const { return *this; }

Point Point::operator +(const Vector& v) const { Point res = *this; return res += v; }
Point Point::operator -(const Vector& v) const { Point res = *this; return res -= v; }

Point& Point::operator +=(const Vector& v) { Vector::operator +=(v); return *this; }
Point& Point::operator -=(const Vector& v) { Vector::operator -=(v); return *this; }

std::ostream& operator <<(std::ostream& out, const Point& v) { return out << ((Vector)v); }