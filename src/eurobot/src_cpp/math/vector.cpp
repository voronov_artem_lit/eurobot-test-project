#include <cmath>
#include <eurobot/math/linalg.hpp>


Vector::Vector() : Vector(0) {}
Vector::Vector(int size, double fill) : Matrix(size, 1, fill) {}
Vector::Vector(const std::vector<double>& data) : Matrix(data.size(), 1, data) {}

void Vector::assert_y(int y) const
{
    if (y < 0 || y >= rows)
        throw std::runtime_error("Vector::assert_y(): y < 0 || y >= rows");
}

double& Vector::at(int y) { assert_y(y); return data[y]; }
double Vector::at(int y) const { assert_y(y); return data[y]; }

std::vector<double>& Vector::get_data() { return data; }
std::vector<double> Vector::get_data() const { return data; }

double Vector::length() const
{
    double len2 = 0;
    for (const double& e : data) len2 += e * e;
    return sqrt(len2);
}

Vector Vector::normalize() const
{
    Vector res = *this;
    double len = res.length();
    if (len <= 0) return res;
    for (double& e : res.data) e /= len;
    return res;
}

Point Vector::to_point() const { return Point(*this); }
Vector Vector::sub_vec(int from, int to) const { return Vector(std::vector<double>(data.begin() + from, data.begin() + to)); }
Matrix Vector::transpose() const
{
    return Matrix(cols, rows, data);
}
Vector Vector::cross3d(const Vector& v) const
{
    if (rows != 3 || v.rows != 3)
        throw std::runtime_error("Vector::cross3d(): rows != 3 || v.rows != 3");

    const std::vector<double>& a = data;
    const std::vector<double>& b = v.data;

    return Vector({
        a[1] * b[2] - a[2] * b[1],
        a[2] * b[0] - a[0] * b[2],
        a[0] * b[1] - a[1] * b[0]
    });
}

Vector Vector::operator -() const
{
    Vector res = *this;
    for (double& e : res.data) e = -e;
    return res;
}

Vector Vector::operator +(double a) const { Vector res = *this; return res += a; }
Vector Vector::operator -(double a) const { Vector res = *this; return res -= a; }
Vector Vector::operator *(double a) const { Vector res = *this; return res *= a; }
Vector Vector::operator /(double a) const { Vector res = *this; return res /= a; }

Vector& Vector::operator +=(double a) { Matrix::operator +=(a); return *this; }
Vector& Vector::operator -=(double a) { Matrix::operator -=(a); return *this; }
Vector& Vector::operator *=(double a) { Matrix::operator *=(a); return *this; }
Vector& Vector::operator /=(double a) { Matrix::operator /=(a); return *this; }

Vector Vector::operator +(const Vector& v) const { Vector res = *this; return res += v; }
Vector Vector::operator -(const Vector& v) const { Vector res = *this; return res += v; }
double Vector::operator *(const Vector& v) const
{
    if (rows != v.rows)
        throw std::runtime_error("Vector::operator *(): rows != v.rows");

    double res = 0;
    for (int i = 0; i < rows; i++)
        res += data[i] * v.data[i];
    return res;
}
Matrix Vector::operator *(const Matrix& m) const { return Matrix::operator *(m); }

Vector& Vector::operator +=(const Vector& v) { Matrix::operator +=(v); return *this; }
Vector& Vector::operator -=(const Vector& v) { Matrix::operator -=(v); return *this; }

std::ostream& operator <<(std::ostream& out, const Vector& v)
{
    out << "[";
    for (int y = 0; y < v.rows; y++)
    {
        if (y != 0) out << ", ";
        out << v.at(y);
    }
    return out << "]";
}