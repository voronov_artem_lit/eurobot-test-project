#include <eurobot/utils/ros/get_param_list_with_log_error.hpp>

ros::V_string get_param_list_with_log_error (
        const ros::NodeHandle& nh,
        const ros::V_string& param_name_list,
        const std::string & prefix,
        const std::string & postfix
    )
{
	for (const auto& name: param_name_list)
		ROS_ERROR_COND(!nh.hasParam(prefix + name + postfix), "Parameter '%s' from param list not found! in '%s'", name.c_str(), nh.getNamespace().c_str());

    ros::V_string answer;    
    answer.reserve(param_name_list.size());

    std::string param;
    for (const auto& name : param_name_list)
    {   
        nh.getParam(prefix + name + postfix, param);     
        answer.emplace_back(std::move(param));
    }
    return answer;
}