#pragma once

#include <ros/node_handle.h>

#include <vector>
#include <string>


ros::V_string get_param_list_with_log_error (
        const ros::NodeHandle& nh,
        const ros::V_string& param_name_list,
        const std::string & prefix  = "",
        const std::string & postfix = ""
    );