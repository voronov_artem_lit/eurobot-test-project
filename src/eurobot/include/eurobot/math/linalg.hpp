#pragma once

#include <vector>
#include <ostream>


class Matrix;
class Vector;
class Point;

namespace AffineMatrix4x4
{

Matrix RotateX(double a);
Matrix RotateY(double a);
Matrix RotateZ(double a);
Matrix TranslateXYZ(double x, double y, double z);

};  // namespace AffineMatrix4x4



class Matrix
{
friend class Vector;

friend Matrix AffineMatrix4x4::RotateX(double a);
friend Matrix AffineMatrix4x4::RotateY(double a);
friend Matrix AffineMatrix4x4::RotateZ(double a);
friend Matrix AffineMatrix4x4::TranslateXYZ(double x, double y, double z);

protected:
    int rows, cols;
    std::vector<double> data;

    void assert_yx(int y, int x) const;
    Matrix(int rows, int cols, const std::vector<double>& data);

public:
    Matrix();
    Matrix(int rows, int cols, double fill=0);

    static Matrix eye(int size);
    
    double& at(int y, int x);
    double at(int y, int x) const;
    std::vector<double>& get_data();
    std::vector<double> get_data() const;

    Matrix minor(int y, int x) const;
    Vector get_row(int y) const;
    Vector get_column(int x) const;
    Matrix transpose() const;

    Matrix operator -() const;

    Matrix operator +(double a) const;
    Matrix operator -(double a) const;
    Matrix operator *(double a) const;
    Matrix operator /(double a) const;

    Matrix& operator +=(double a);
    Matrix& operator -=(double a);
    Matrix& operator *=(double a);
    Matrix& operator /=(double a);

    Matrix operator +(const Matrix& m) const;
    Matrix operator -(const Matrix& m) const;
    Matrix operator *(const Matrix& m) const;

    Matrix& operator +=(const Matrix& m);
    Matrix& operator -=(const Matrix& m);
    Matrix& operator *=(const Matrix& m);

    friend std::ostream& operator <<(std::ostream& out, const Matrix& m);
};



class Vector : protected Matrix
{
protected:
    void assert_y(int y) const;

public:
    Vector();
    explicit Vector(int size, double fill=0);
    explicit Vector(const std::vector<double>& data);

    double& at(int y);
    double at(int y) const;
    std::vector<double>& get_data();
    std::vector<double> get_data() const;

    double length() const;
    Vector normalize() const;
    Point to_point() const;
    Vector sub_vec(int from, int to) const;
    Matrix transpose() const;
    Vector cross3d(const Vector& v) const;

    Vector operator -() const;

    Vector operator +(double a) const;
    Vector operator -(double a) const;
    Vector operator *(double a) const;
    Vector operator /(double a) const;

    Vector& operator +=(double a);
    Vector& operator -=(double a);
    Vector& operator *=(double a);
    Vector& operator /=(double a);

    Vector operator +(const Vector& v) const;
    Vector operator -(const Vector& v) const;
    double operator *(const Vector& v) const;
    Matrix operator *(const Matrix& m) const;

    Vector& operator +=(const Vector& v);
    Vector& operator -=(const Vector& v);

    friend std::ostream& operator <<(std::ostream& out, const Vector& v);
};



class Point : protected Vector
{
private:
    friend class Vector;

    explicit Point(const Vector& v);

protected:
    void assert_y(int y) const;

public:
    Point();
    explicit Point(int size, double fill=0);

    double& at(int y);
    double at(int y) const;
    std::vector<double>& get_data();
    std::vector<double> get_data() const;

    Vector to_vector() const;

    Point operator +(const Vector& v) const;
    Point operator -(const Vector& v) const;

    Point& operator +=(const Vector& v);
    Point& operator -=(const Vector& v);

    friend std::ostream& operator <<(std::ostream& out, const Point& v);
};