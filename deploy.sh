#!/bin/bash

ENABLE_AUTOUPDATE=true
CONTAINER_NAME=ros_deploy
DOCKER_IMAGE="markovvn1/ros_deploy_eurobot2021"
set -e

realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

show_instructions() {
	echo -e "Use '\e[0;34mdocker exec -it ${CONTAINER_NAME} bash\e[0m' to start deploying"
	echo -e "Use '\e[0;34mdocker exec -it ${CONTAINER_NAME} ssh TARGET\e[0m' to connect to target machine"
}

if [ "$(docker ps -a | grep ${CONTAINER_NAME})" ]; then
	echo -e "\e[1;32mContainer with name ${CONTAINER_NAME} already exist\e[0m"
	show_instructions
	exit 1
fi


if [[ $EUID -eq 0 ]]; then
	USER=$SUDO_USER
else
	USER=$USER
fi
USER_HOME=`getent passwd $USER | cut -d: -f6`


HOME_PATH="$(dirname "$(realpath "$0")")"
ID_RSA_PATH="$USER_HOME/.ssh/eurobot_2021_id_rsa"


if [ ! -d "${HOME_PATH}/src" ]; then
	echo "'src' folder not found"
	exit 1
fi
if [ ! -f "$ID_RSA_PATH" ]; then
	echo "id_rsa file not found. Ask the administrator for help"
	exit 1
fi


# pull image (check updates)
if [[ "$(uname -m)" == "x86_64" ]] && $ENABLE_AUTOUPDATE; then
	echo -e "\e[0;32mChecking updates...\e[0m"
	docker pull $DOCKER_IMAGE || echo -e "\e[0;33mWarning: failed to update docker image\e[0m"
else
	echo -e "\e[0;33mWarning: auto updates disabled\e[0m"
fi



DOCKER_ARGS=(-d -it --rm -h deploy --name ${CONTAINER_NAME})
DOCKER_ARGS+=(-e "PC_HOSTNAME=`hostname`" -v "/tmp/.X11-unix:/tmp/.X11-unix:rw" -v "/etc/timezone:/etc/timezone:ro" -v "/etc/localtime:/etc/localtime:ro")
DOCKER_ARGS+=(--add-host=robot1:192.168.16.12)

docker run ${DOCKER_ARGS[@]} -v "$HOME_PATH/src:/home/app/catkin_ws/src/src:ro" $DOCKER_IMAGE
docker exec ${CONTAINER_NAME} sh -c "echo \"$(<"$ID_RSA_PATH")\" > /home/app/.ssh/id_rsa"
docker exec ${CONTAINER_NAME} chmod 400 /home/app/.ssh/id_rsa

echo -e "\e[1;32mDocker have started\e[0m"
show_instructions
