#!/bin/bash

ENABLE_AUTOUPDATE=true
CONTAINER_NAME=ros_gaz_env_mv
DOCKER_IMAGE="markovvn1/ros_gaz_env_eurobot2021"
set -e

realpath() {  # for MAC OS
	[[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

say_help() {
	echo "Using: $0 {intel|nvidia|amd|none}"
	echo "For example, if you have nvidia driver, then use $0 nvidia"
}

show_instructions() {
	echo -e "Use '\e[0;34mdocker exec -it ${CONTAINER_NAME} bash\e[0m' to connect"
	echo -e "Use '\e[0;34mdocker stop ${CONTAINER_NAME}\e[0m' to stop container"
}

if [ $# -eq 0 ] || [ $1 == "-h" ] || [ $1 == "--help" ]; then
	say_help
	exit
fi

if [ "$(docker ps -a | grep ${CONTAINER_NAME})" ]; then
	echo -e "\e[1;32mContainer with name ${CONTAINER_NAME} already exist\e[0m"
	show_instructions
	exit 1
fi

HOME_PATH="$(dirname "$(realpath "$0")")"

if [ ! -d "${HOME_PATH}/src" ]; then
	echo "'src' folder not found"
	exit 1
fi

if [[ "$OSTYPE" == "darwin"* ]]; then
	USER_UID=$(stat -f '%u' "${HOME_PATH}/src")
	USER_GID=$(stat -f '%g' "${HOME_PATH}/src")
else
	USER_UID=$(stat -c '%u' "${HOME_PATH}/src")
	USER_GID=$(stat -c '%g' "${HOME_PATH}/src")
fi


# pull image (check updates)
if [ $USER_UID -eq 1000 ] && [ $USER_GID -eq 1000 ] && [[ "$(uname -m)" == "x86_64" ]] && $ENABLE_AUTOUPDATE; then
	echo -e "\e[0;32mChecking updates...\e[0m"
	docker pull $DOCKER_IMAGE || echo -e "\e[0;33mWarning: failed to update docker image\e[0m"
else
	echo -e "\e[0;33mWarning: auto updates disabled\e[0m"
fi

if [[ "$(docker images -q --filter "label=non_root_permissions=$USER_UID:$USER_GID" markovvn1/ros_gaz_env_eurobot2021 | wc -l)" == '0' ]]; then
	echo -e "\e[1;31mImage markovvn1/ros_gaz_env_eurobot2021 with permissions $USER_UID:$USER_GID not found\e[0m"
	echo "Probably you need to compile it by yourself"
	exit
fi



DOCKER_ARGS=(-d -it --rm -h eurobot --name ${CONTAINER_NAME})
DOCKER_ARGS+=(-e "DISPLAY=${DISPLAY}" -v "/tmp/.X11-unix:/tmp/.X11-unix:rw")
DOCKER_ARGS+=(--add-host=robot1-eurobot:192.168.16.12 --add-host=lighthouse-eurobot:192.168.16.13)
SRC_VOLUME="${HOME_PATH}/src:/home/app/catkin_ws/src/eurobot"

if [ $1 == "intel" ]; then
	DOCKER_ARGS+=(--device="/dev/dri:/dev/dri")
elif [ $1 == "amd" ]; then
	DOCKER_ARGS+=(--device="/dev/dri:/dev/dri" --group-add video)
elif [ $1 == "nvidia" ]; then
	DOCKER_ARGS+=(--runtime nvidia)
elif [ $1 != "none" ]; then
	echo "Unknown first argument"
	say_help
	exit
fi


if [ $# -ge 2 ] && [ $2 == "usb" ]; then
	DOCKER_ARGS+=(-v "/dev/bus/usb:/dev/bus/usb" --privileged)
	DOCKER_ARGS+=(${@:3})
else
	DOCKER_ARGS+=(${@:2})
fi


echo "Run docker with args: ${DOCKER_ARGS[@]}"

docker run ${DOCKER_ARGS[@]} -v "$SRC_VOLUME" $DOCKER_IMAGE > /dev/null && \
echo -e "\e[1;32mDocker have started\e[0m"
show_instructions